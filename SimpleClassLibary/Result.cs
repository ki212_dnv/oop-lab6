﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClassLibary
{
    public class Result
    {
        private string subject;
        private string teacher;
        private int points;
        public Result()
        {
            subject = "Unknown";
            teacher = "Unkown";
            points = 0;
        }
        public Result(string subject, int points)
        {
            this.subject = subject;
            this.points = points;
        }
        public Result(string subject, int points, string teacher)
        {
            this.subject = subject;
            this.points = points;
            this.teacher = teacher;
        }
        public Result(Result previousResult)
        {
            subject = previousResult.subject;
            points = previousResult.points;
            teacher = previousResult.teacher;
        }
        public string Subject
        {
            get
            {
                return subject;
            }
            set
            {
                subject = value;
            }
        }
        public string Teacher
        {
            get
            {
                return teacher;
            }
            set
            {
                teacher = value;
            }
        }
        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                points = value;
            }
        }

    }
}
