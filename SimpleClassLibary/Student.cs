﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClassLibary
{
    public class Student
    {
        private string name;
        private string surname;
        private int group;
        private int year;
        public double Bucks { get { return Bucks; } set {
                Bucks = value;
                Bucks2 = value * 10;
                Bucks3 = value * 40;
            } }
        public double Bucks2 { get { return Bucks2; } set {
                Bucks2 = value;
                Bucks = value / 10;
                Bucks3 = value * 4;
            } }
        public double Bucks3 { get { return Bucks3; } set {
                Bucks3 = value;
                Bucks = value / 40;
                Bucks2 = value / 4;
            } }
        List<Result> results=new List<Result>();
        public Student()
        {
            name = "Unknown";
            surname = "Unknown";
            group = 0;
            year = 0;
            results = new List<Result>();
        }
        public Student(string name, int year)
         {
            this.name = name;
            this.year = year;
         }
        public Student(string name, string surname, int group, int year,List<Result> Results)
        {
            this.name = name;
            this.year = year;
            this.surname = surname;
            this.group = group;
            results = Results;
        }
        public Student (Student previousStudent)
        {
            name = previousStudent.name;
            year = previousStudent.year;
            surname = previousStudent.surname;
            group = previousStudent.group;
            results = previousStudent.results;
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }
        public int Group
        {
            get
            {
                return group;
            }
            set
            {
                group = value;
            }
        }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }
        public List<Result> Results
        {
            get
            {
                return results;
            }
            set
            {
                results = value;
            }
        }
        public double GetAveragePoints()
        {
            double sum=0; 
            for (int i = 0; i < results.Count; i++)
            {
                sum +=results[i].Points;
            }
           double everege=sum/results.Count;
            return everege;

        }
        public string GetBestSubject()
        {
            int indMax=0, max=results[0].Points;
            for(int i=0; i<results.Count; i++)
            {
                if (results[i].Points > max) {
                    max = results[i].Points;
                    indMax = i;
                }
            }
            return results[indMax].Subject;
        }
        public string GetWorstSubject()
        {
            int indMin = 0, min = results[0].Points;
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].Points <min)
                {
                    min = results[i].Points;
                    indMin = i;
                }
            }
            return results[indMin].Subject;
        }


    }
}


