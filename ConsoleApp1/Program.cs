﻿using System;
using System.Collections.Generic;
using SimpleClassLibary;

namespace ConsoleApp1
{
    class Program
    {
        public static Student[] ReadProductsArray()
        {
            int n = int.Parse(Console.ReadLine());
            Student[] students = new Student[n];
            string name;
            string surname;
            int group;
            int year;
            int hhh;
            List<Result> results = new List<Result>();

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Введіть ім'я");
                name = Console.ReadLine();
                Console.WriteLine("Введіть прізвище");
                surname = Console.ReadLine();
                Console.WriteLine("Введіть номер групи");
                group = int.Parse(Console.ReadLine());
                Console.WriteLine("Введіть курс");
                year = int.Parse(Console.ReadLine());

                Console.WriteLine("Введіть скільки предметів ви хочете ввести");
                int count = int.Parse(Console.ReadLine());
                for (int j = 0; j < count; j++)
                {
                    string SubjectsName;
                    string TeacherName;
                    int Points;
                    Console.WriteLine("Subject Name");
                    SubjectsName = Console.ReadLine();
                    Console.WriteLine("Teacher Name");
                    TeacherName = Console.ReadLine();
                    Console.WriteLine("Subject Point");
                    Points = int.Parse(Console.ReadLine());
                    results.Add(new Result(SubjectsName, Points, TeacherName));
                }
                students[i] = new Student(name, surname, group, year, results);
                Console.WriteLine("За який період вказати ціну начання \n1-місяць \n2-рік \n3-весь період");
                hhh = int.Parse(Console.ReadLine());
                double cost;
                Console.WriteLine("Вкажіть ціну");
                cost= double.Parse(Console.ReadLine());
                switch (hhh)
                {
                    case 1:
                        {
                            students[i].Bucks=cost;
                            break;
                        }
                    case 2:
                        {
                            students[i].Bucks2 = cost;
                            break;
                        }
                    case 3:
                        {
                            students[i].Bucks3 = cost;
                            break;
                        }
                }
                results = new List<Result>();

            }
            return students;
        }
        public static void PrintStudent(Student student)
        {
            int hhh;
            Console.WriteLine($"Ім'я:\n{student.Name}" +
                $"\n Прізвище:{student.Surname}" +
                $"\n Номер групи:{student.Group}" +
                $"\n Курс:{student.Year}");
            Console.WriteLine("За який період ви бажаєте вивести \n1-місяць \n2-рік \n3-весь період");
            hhh = int.Parse(Console.ReadLine());
            switch (hhh)
            {
                case 1:
                    {
                        Console.WriteLine(student.Bucks);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine(student.Bucks2);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine(student.Bucks3);
                        break;
                    }
            }

            foreach (var result in student.Results)
            {
                Console.WriteLine($" \nПредмет {result.Subject}" +
                    $"\nВчитель {result.Teacher}" +
                    $"\nБал {result.Points}");
            }


        }
        public static void PrintStudents(Student[] student)
        {
            
            Console.WriteLine("За який період ви бажаєте вивести \n1-місяць \n2-рік \n3-весь період");
            int hhh = int.Parse(Console.ReadLine());
            for (int i = 0; i < student.Length; i++)
            {
                Console.WriteLine($"Ім'я:\n{student[i].Name}" +
                $"\n Прізвище:{student[i].Surname}" +
                $"\n Номер групи:{student[i].Group}" +
                $"\n Курс:{student[i].Year}");
               
            switch (hhh)
            {
                case 1:
                    {
                        Console.WriteLine(student[i].Bucks);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine(student[i].Bucks2);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine(student[i].Bucks3);
                        break;
                    }
            }

                foreach (var result in student[i].Results)
                {
                    Console.WriteLine($" \nПредмет {result.Subject}" +
                        $"\nВчитель {result.Teacher}" +
                        $"\nБал {result.Points}");
                }
            }

        }
        public static void GetStudentsInfo(Student[] students, out double max, out double min)
        {
            double Min = students[0].GetAveragePoints();
            double Max = students[0].GetAveragePoints();
            for (int i = 0; i < students.Length; i++)
            {
                if (students[i].GetAveragePoints() > Max)
                {
                    Max = students[i].GetAveragePoints();
                }
                if (students[i].GetAveragePoints() < Min)
                {
                    Min = students[i].GetAveragePoints();
                }
            }
            min = Min;
            max = Max;
        }
        public static int SortStudentsByPoint(Student student1, Student student2)
        {
            double Point1 = student1.GetAveragePoints(), Point2 = student2.GetAveragePoints();
            if (Point1 > Point2)
            {
                return 1;
            }
            else if (Point1 < Point2)
            {
                return -1;
            }
            return 0;

        }

        public static int SortStudentsBySurname(Student student1, Student student2)
        {
            string NameSurname = student1.Surname, SecondSurname = student2.Surname;
            if (string.Compare(NameSurname, SecondSurname) == 1)
            {
                return 1;
            }
            else if (string.Compare(NameSurname, SecondSurname) == -1)
            {
                return -1;
            }
            return SortStudentsByName(student1, student2);
        }
        public static int SortStudentsByName(Student student1, Student student2)
        {
            string NameFirst = student1.Name, SecondName = student2.Name;
            if (string.Compare(NameFirst, SecondName) == 1)
            {
                return 1;
            }
            else if (string.Compare(NameFirst, SecondName) == -1)
            {
                return -1;
            }
            return 0;
        }





        static void Main(string[] args)
        {
            Console.Title = "Лабораторна робота №6";
            Student[] students = ReadProductsArray();

            int Menu = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("0. Output one of students\n1.All Student\n2.Sort By Points\n3.Sort by Name \n");
                do
                {
                    try
                    {
                        Menu = int.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        Menu = -1;
                    }
                } while (Menu == -1);
                switch (Menu)

                {
                    case 0:
                        {
                            Console.WriteLine("which entrant index");
                            do
                            {
                                try
                                {
                                    Menu = int.Parse(Console.ReadLine());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            } while (Menu >= students.Length && Menu < 0);
                            PrintStudent(students[Menu]);
                            break;
                        }
                    case 1:
                        {
                            PrintStudents(students);
                            break;
                        }
                    case 2:
                        {
                            Array.Sort(students, SortStudentsByPoint);
                            break;
                        }
                    case 3:
                        {
                            Array.Sort(students, SortStudentsByName);
                            break;
                        }
                    case 4:
                        {
                            GetStudentsInfo(students, out double min, out double max);
                            Console.WriteLine($"Min info:{min}\nMax info:{max}");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Choose one of ");
                            break;
                        }
                }
                Menu = int.Parse(Console.ReadLine());
            } while (Menu != -1);
        }
    }
}









